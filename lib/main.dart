import 'package:flutter/material.dart';

enum APP_THEME{LIGHT,DARK}

void main() {
  runApp(ContactProfilePage());
}
class MyAppTheme{
  static ThemeData appThemeLight(){
    return ThemeData(
      brightness: Brightness.light,
      appBarTheme: AppBarTheme(
        color: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.pink.shade200,
      ),
    );
  }
  static ThemeData appThemeDark(){
    return ThemeData(
      brightness: Brightness.dark,
      appBarTheme: AppBarTheme(
        color: Colors.black,
        iconTheme: IconThemeData(
          color: Colors.pink,
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.pink.shade200,

      ),
    );
  }
}
class ContactProfilePage extends StatefulWidget{
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIGHT;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme == APP_THEME.DARK
        ? MyAppTheme.appThemeLight()
        : MyAppTheme.appThemeDark(),

      home: Scaffold(
         appBar: buildAppBarWidget(),
         body: buildBodyWidget(),


        floatingActionButton: FloatingActionButton(
          backgroundColor:  Colors.red.shade500,

          foregroundColor: Colors.yellow.shade500,
          child: Icon(Icons.sunny),
          onPressed: () {
            setState(() {

              currentTheme == APP_THEME.DARK
                  ? currentTheme = APP_THEME.LIGHT
                  : currentTheme = APP_THEME.DARK;
            });
          },
        ),
      ),
    );
  }
}
Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}
Widget buildCallButton2() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.chat,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],
  );
}
Widget buildCallButton3() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Video"),
    ],
  );
}
Widget buildCallButton4() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],
  );
}
Widget buildCallButton5() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Directions"),
    ],
  );
}
Widget buildCallButton6() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Pay"),
    ],
  );
}

Widget mobilePhoneListTile(){
  return ListTile(
    leading: Icon(Icons.call),
    title: Text("330-803-3390"),
    subtitle: Text("mobile"),
    trailing: IconButton(
      icon: Icon(Icons.message),
     color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}
Widget otherPhoneListTile(){
  return ListTile(
    leading: Text(""),
    title: Text("440-440-3390"),
    subtitle: Text("other"),
    trailing: IconButton(
      icon: Icon(Icons.message),
       color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}
Widget emailListTile(){
  return ListTile(
    leading: Icon(Icons.email),
    title: Text("priyanka@priyanka.com"),
    subtitle: Text("work"),
  );
}
Widget addressListTile(){
  return ListTile(
    leading: Icon(Icons.location_on),
    title: Text("234 Sunset St.Burlingame"),
    subtitle: Text("home"),
    trailing: IconButton(
      icon: Icon(Icons.directions),
      color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}
buildAppBarWidget() {
return AppBar(
    backgroundColor: Colors.pink.shade200,
    leading: Icon(
        Icons.arrow_back,
        color: Colors.black),
    actions: <Widget>[
      IconButton(
        icon: Icon(Icons.star_border),
        color: Colors.black,
        onPressed: (){
          print("Contact is starred");
        },
      ),
    ],
  );
}
buildBodyWidget() {
  return ListView(
    children: <Widget>[
      Column (
        children: <Widget>[
          Container(
            width: double.infinity,
            height: 250,
            child: Image.network(
              "https://i.pinimg.com/564x/f6/dc/36/f6dc364cca06cda19f8f299623a74b6c.jpg",
              fit: BoxFit.fill,
            ),
          ),
          Container(
            height: 60,
            child:Row(
              mainAxisAlignment: MainAxisAlignment.start,

              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child :Text(
                    "Pariyakorn Dangkrajang",
                    style: TextStyle(fontSize:30),
                  ),
                ),
              ],
            ),
          ),


          Container(
            margin: const EdgeInsets.only(top: 8,bottom: 8),
            child: Theme(
              data: ThemeData (
                iconTheme: IconThemeData(
                  color: Colors.pink.shade200,
                ),
              ),
              child: profileActionItems(),
            ),

          ),
          Divider(
            color: Colors.grey,
          ),
          mobilePhoneListTile(),
          otherPhoneListTile(),

          Divider(
            color: Colors.grey,
          ),
          emailListTile(),
          Divider(
            color: Colors.grey,
          ),
          addressListTile()
        ],
      ),
    ],
  );
}
Widget profileActionItems(){
  return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children:<Widget> [
        buildCallButton(),
        buildCallButton2(),
        buildCallButton3(),
        buildCallButton4(),
        buildCallButton5(),
        buildCallButton6(),
      ],
  );
}